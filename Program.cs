﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;

class Program {

    private static string _applicationPath;
    private static string AssemblerPath {
        get { return _applicationPath + @"\binaries\ASEMW.EXE"; }
    }
    private static string EmulatorPath {
        get { return _applicationPath + @"\binaries\DSM-51_Any_CPU.exe"; }
    }

    private static FileInfo _sourceFile;
    private static string SourceDirectory {
        get { return _sourceFile.DirectoryName; }
    }
    private static string SourceFileName {
        get { return Path.GetFileNameWithoutExtension(_sourceFile.Name); }
    }

    static bool AreBinariesPresent() {
        _applicationPath = Path.GetDirectoryName(Assembly.GetAssembly(typeof(Program)).Location);

        if (File.Exists(AssemblerPath) == false) {
            Console.WriteLine("{0} nie został znaleziony.", Path.GetFileName(AssemblerPath));
            return false;
        }
        if (File.Exists(EmulatorPath) == false) {
            Console.WriteLine("{0} nie został znaleziony.", Path.GetFileName(EmulatorPath));
            return false;
        }
        return true;
    }

    static bool Initialize(string path) {
        _sourceFile = new FileInfo(path);
        return _sourceFile.Exists;
    }

    static void WaitForInputAndClose() {
        Console.ReadKey();
        Environment.Exit(0);
    }

    static void Main(string[] args) {
        if (AreBinariesPresent() == false) WaitForInputAndClose();

        if (args == null || args.Length == 0) {
            Console.WriteLine("Nie przekazano żadnych argumentów.");
            WaitForInputAndClose();
        }

        if (Initialize(args[0]) == false) {
            Console.WriteLine("Podany plik nie istnieje.");
            WaitForInputAndClose();
        }

        // Rozpocznij assemblację podanego programu i wstrzymaj program do jego zakończenia
        var assembleProcess = Process.Start(new ProcessStartInfo { FileName = AssemblerPath, Arguments = "\"" + _sourceFile.FullName + "\"", CreateNoWindow = true, WindowStyle = ProcessWindowStyle.Hidden });
        Console.WriteLine("Trwa assemblacja pliku {0}...", _sourceFile.Name);
        assembleProcess.WaitForExit();

        // Uruchom listing zassemblowanego pliku jeśli wystąpiły w nim błędy
        if (assembleProcess.ExitCode == 1) {
            var assembledListingFilePath = SourceDirectory + "\\" + SourceFileName + ".lst";
            Process.Start(new ProcessStartInfo { FileName = "notepad.exe", Arguments = "\"" + assembledListingFilePath + "\"" });
            return;
        } 
        // Wyświetl informację o problemie, jeśli wystąpił błąd inny niż błąd składni .asm
        if (assembleProcess.ExitCode > 1) {
            Console.WriteLine("Assembler nie mógł zassemblować pliku.");
            WaitForInputAndClose();
        }

        // Otwórz utworzony plik .hex w emulatorze i zakończ program
        var hexFilePath = SourceDirectory + "\\" + SourceFileName + ".hex";
        Process.Start(new ProcessStartInfo { FileName = EmulatorPath, Arguments = "\"" + hexFilePath + "\"" });
    }
}